#!/usr/bin/python
# -*- coding: utf-8 -*-

from __future__ import division

import os
import pdb

from collections import (
    deque,
    namedtuple,
)

from functools import partial

from typing import (
    Any,
    Iterator,
    List,
    Tuple,
)

import numpy as np


Item = namedtuple("Item", ['index', 'value', 'weight'])


def parse_input(input_data: str) -> Tuple[List[Item], int]:
    lines = input_data.split('\n')

    firstLine = lines[0].split()
    item_count = int(firstLine[0])
    capacity = int(firstLine[1])

    items = []

    for i in range(1, item_count+1):
        line = lines[i]
        parts = line.split()
        items.append(Item(i-1, int(parts[0]), int(parts[1])))

    return items, capacity


def prepare_output(items: List[Item], taken: List[bool]) -> str:
    value = sum(item.value for i, item in enumerate(items) if taken[i])
    weight = sum(item.weight for i, item in enumerate(items) if taken[i])
    output_data = str(value) + ' ' + str(0) + '\n'
    output_data += ' '.join(map(lambda t: "1" if t else "0", taken))
    return output_data


def greedy(items: List[Item], capacity: int, sort_func: Any) -> List[bool]:
    # A trivial greedy algorithm for filling the knapsack:
    # it takes items in-order until the knapsack is full
    value = 0
    weight = 0
    taken = [False] * len(items)

    if sort_func:
        items = sorted(items, key=sort_func)

    for item in items:
        if weight + item.weight <= capacity:
            taken[item.index] = True 
            value += item.value
            weight += item.weight
   
    return taken


def dynamic_programming(items: List[Item], capacity: int) -> List[bool]:

    nr_items = len(items)
    f = np.zeros((capacity + 1, nr_items), dtype=np.int)

    # Bottom-up solution
    for j, item in enumerate(items):
        for i in range(1, capacity + 1):
            if item.weight > i:
                f[i, j] = f[i, j - 1]
            else:
                values = f[i, j - 1], item.value + f[i - item.weight, j - 1]
                f[i, j] = max(values)

    # Decoding
    i = capacity
    taken = [False] * nr_items

    for j in reversed(range(nr_items)):
        if j == 0:
            taken[j] = f[i, j] > 0 and i > 0
        elif f[i, j] > f[i, j - 1]:
            taken[j] = True
            i = i - items[j].weight

    value_taken = sum(item.value for is_taken, item in zip(taken, items) if is_taken)
    value_last = f[capacity, nr_items - 1]
    assert value_taken == value_last

    return taken


def branch_and_bound(items: List[Item], capacity: int, verbose: int=0) -> List[bool]:

    Node = namedtuple("Node", ['selection', 'score', 'value', 'weight'])
    items = sorted(
        items,
        key=lambda item: item.value / item.weight,
        reverse=True,
    )
    nr_items = len(items)

    def compute_relaxed_value(curr_index: int, curr_weight: int, curr_value: float) -> float:
        if curr_weight > capacity:
            return -np.inf

        value = curr_value
        weight = curr_weight

        for item in items[curr_index:]:
            if weight + item.weight <= capacity:
                value += item.value
                weight += item.weight
            else:
                value += item.value * (capacity - weight) / item.weight
                break

        return value

    def expand(node: Node) -> Iterator[Node]:
        for to_select in (False, True):
            index = len(node.selection)
            selection = node.selection + [to_select]
            value = node.value + (items[index].value if to_select else 0)
            weight = node.weight + (items[index].weight if to_select else 0)
            score = compute_relaxed_value(index + 1, weight, value)
            yield Node(
                selection=selection,
                score=score,
                value=value,
                weight=weight,
            )

    def is_solution(node: Node) -> bool:
        return len(node.selection) == nr_items

    def node_to_str(node: Node) -> str:
        return ' '.join([
            '{:10.2f}'.format(node.score),
            '{:10.2f}'.format(node.value),
            '{:10d}'.format(node.weight),
            ''.join('+' if s else '.' for s in node.selection),
        ])

    frontier = [
        Node(
            selection=[],
            value=0,
            weight=0,
            score=compute_relaxed_value(0, 0, 0),
        ),
    ]

    frontier = deque(frontier)

    none_taken = [False] * nr_items
    best_solution = Node(selection=none_taken, score=0, value=0, weight=0)

    nr_nodes = 1
    nr_unpruned = 1

    while frontier:
        node = deque.pop(frontier)

        if verbose > 1:
            print(
                '{:10d}'.format(nr_nodes),
                node_to_str(node),
            )

        for child in expand(node):
            nr_nodes += 1
            if is_solution(child) and child.score > best_solution.score:
                best_solution = child
                if verbose:
                    print(
                        '{:10d}'.format(nr_nodes),
                        node_to_str(best_solution),
                    )
            elif child.score > best_solution.score:
                frontier.append(child)
                nr_unpruned += 1

    indexes = [item.index for item in items]
    taken = [s for _, s in sorted(zip(indexes, best_solution.selection))]

    if verbose:
        print("Nr nodes:", nr_nodes)
        print("Unpruned:", nr_unpruned)

    return taken


def adaptive_algo(items: List[Item], capacity: int) -> List[bool]:
    MAX_SIZE = 20000000
    problem_size = len(items) * capacity
    if problem_size <= MAX_SIZE:
        print('Using dynamic programming')
        return dynamic_programming(items, capacity)
    else:
        print('Using greedy algorithm')
        return greedy(items, capacity, lambda item: - item.value / item.weight)


ALGOS = {
    'greedy': partial(greedy, sort_func=None),
    'greedy_value': partial(greedy, sort_func=lambda item: - item.value),
    'greedy_value_weight': partial(greedy, sort_func=lambda item: - item.value / item.weight),
    'dynamic_programming': dynamic_programming,
    'branch_and_bound': branch_and_bound,
    'adaptive': adaptive_algo,
}


def solve_it(input_data):
    items, capacity = parse_input(input_data)
    taken = ALGOS[os.environ['ALGO']](items, capacity)
    return prepare_output(items, taken)


if __name__ == '__main__':
    import sys
    if len(sys.argv) > 1:
        file_location = sys.argv[1].strip()
        with open(file_location, 'r') as input_data_file:
            input_data = input_data_file.read()
        print(solve_it(input_data))
    else:
        print('This test requires an input file.  Please select one from the data directory. (i.e. python solver.py ./data/ks_4_0)')

